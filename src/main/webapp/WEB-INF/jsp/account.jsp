<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/12/21 0021
  Time: 21:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>账号设置 - 灵步微博</title>
    <link href="/styles/global.css" type="text/css" rel="stylesheet" />
    <link href="/styles/account.css" type="text/css" rel="stylesheet" />
    <script src="/script/sitedata_bas.js" language="javascript"></script>
</head>

<body>

<form id="form1" name="form1" method="post"action="/account" >
    <!-- container部分DIV -->
    <div id="container">
        <!-- banner部分DIV -->
        <div id="banner">
            <!-- banner部分的leftDIV -->
            <div class="left" id="left">
                <table width="564"  height="570" border="0" cellpadding="0" cellspacing="0" class="left">
                    <!-- 用户名 -->
                    <tr>
                        <td width="120" height="50" align="right"><strong>用户名</strong></td>
                        <td width="20" height="50">&nbsp;</td>
                        <td width="425" height="50"><label>
                            <input name="userName" type="text" class="n1" id="userName" value="${Login_User.name}" readonly="readonly"  />
                            </label></td>
                    </tr>
                    <!-- 昵称 -->
                    <tr>
                        <td width="120" height="50" align="right"><strong>昵称</strong></td>
                        <td width="20" height="50">&nbsp;</td>
                        <td width="425" height="50"><label>
                            <input name="textfield" type="text" class="n1" id="textfield" value="${Login_User.nick_name}"  />
                            </label>
                        </td>
                    </tr>
                    <!-- 个性域名 -->
                    <tr>
                        <td width="120" height="50" align="right"><strong>个性域名</strong></td>
                        <td width="20" height="50">&nbsp;</td>
                        <td width="425" height="50"><label>
                            <input name="domainhacks" type="text" class="n2" id="domainhacks"
                                   value="${Login_User.domainhacks}"   />
                            </label></td>
                    </tr>
                    <!-- 邮箱 -->
                    <tr>
                        <td width="120" height="49" align="right"><strong>邮箱</strong></td>
                        <td width="20" height="49">&nbsp;</td>
                        <td width="425" height="49"><label>
                            <input name="textfield3" type="text" class="n3" id="textfield3" value="${Login_User.mail}" />
                        </label></td>
                    </tr>
                    <!-- 地址 -->
                    <tr>
                        <td width="120" height="52" align="right"><strong>地址</strong></td>
                        <td width="20" height="52">&nbsp;</td>
                        <td width="425" height="52">
                            <label>
                                <select name="region1" class="tb" id="region1"></select>
                            </label>
                            <label>
                                <select name="region2" class="tb" id="region2"></select>
                                <select name="region3" class="tb" id="region3"></select>
                            </label>
                        </td>
                    </tr>
                    <!-- 个人站点 -->
                    <tr>
                        <td width="120" height="68" align="right"><strong>个人站点</strong></td>
                        <td width="20" height="68">&nbsp;</td>
                        <td width="425" height="68">
                            <label>
                                <input name="personalSite" type="text" class="n1" id="personalSite"
                                       value="${Login_User.personalSite}" />

                            </label>
                        </td>
                    </tr>
                    <!-- 个性签名 -->
                    <tr>
                        <td width="120" height="130" align="right"><strong>个性签名</strong></td>
                        <td width="20" height="130">&nbsp;</td>
                        <td width="425" height="130">
                            <div>
                                <label>
                                    <textarea name="textfield5" class="n4" id="textfield5" value="${Login_User.personalSite}"></textarea>
                                </label>
                            </div></td>
                    </tr>
                    <!-- 隐私 -->
                    <tr>
                        <td width="120" height="29" align="right"><strong>隐私</strong></td>
                        <td width="20" height="29">&nbsp;</td>
                        <td width="425" height="29"><label>
                            <input name="sec" type="radio" id="radio" value="${Login_User.sec}" checked="checked" />
                            所有人可见
                            <input type="radio" name="sec" id="radio2" value="${Login_User.sec}" />
                            关注我的人可见
                        </label></td>
                    </tr>
                    <!-- 保存按钮 -->
                    <tr>
                        <td width="120" height="44" align="right">&nbsp;</td>
                        <td width="20" height="44">&nbsp;</td>
                        <td width="425" height="44"><label>
                            <input name="button" type="submit" class="btn" id="button" value="保存" />
                        </label></td>
                    </tr>
                </table>
            </div>
            <!-- banner_left部分DIV结束 -->
            <!-- banner_right部分DIV -->
            <div class="right" id="right">
                <p>在这里
                    ，你可以设置你账号的基本信息，隐私信息等</p>
            </div>
            <!-- banner_right部分DIV结束 -->
        </div>
        <!-- banner部分DIV结束 -->
    </div>
    <!-- container部分DI结束V -->
</form>
</body>
</html>
