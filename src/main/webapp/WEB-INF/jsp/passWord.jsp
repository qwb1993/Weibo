<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/12/21 0021
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>密码设置 - 灵步微博</title>
    <link href="/styles/global.css" type="text/css" rel="stylesheet" />
    <link href="/styles/password.css" type="text/css" rel="stylesheet" />
    <script src="/script/register.js" language="javascript"></script>
    <script src="/script/sitedata_bas.js" language="javascript"></script>
    <script src="/script/datecreate.js" language="javascript"></script>
    <script src="/script/trim.js" language="javascript"></script>
</head>

<body>
<form id="form1" name="form1" method="post" action="/passWord">
    <!-- container部分DIV -->
    <div id="container">
        <!-- banner部分DIV -->
        <div id="banner">
            <!-- bannerLeft部分DIV -->
            <div class="left" id="left">
                <table width="564" border="0" cellspacing="0" cellpadding="0">
                    <!-- 当前密码 -->
                    <tr>
                        <td width="120" height="65" align="right"><strong>当前密码</strong></td>
                        <td width="20" height="65">&nbsp;</td>
                        <td width="210" height="65"><label>
                            <input name="oldPass" type="text" class="n1" id="oldPass" class="form" value="${Login_User.password}"
                                   onfocus="getfocus(this,img5)" onblur="checkUserPass(img5,this)" />
                        </label>
                        </td>
                        <td width="25" height="50" align="left" valign="middle" class="wordright">
                            <img name="img4" width="16" height="16" id="img4" /></td>
                        <td width="170" height="65">
                        </td>
                    </tr>
                    <!-- 新密码 -->
                    <tr>
                        <td width="120" height="65" align="right"><strong>新密码</strong></td>
                        <td width="20" height="65">&nbsp;</td>
                        <td width="210" height="65"><label>
                            <input name="userPass" type="password" class="n1" id="userPass" class="form" onfocus="getfocus(this,img5)" onblur="checkUserPass(img5,this)" />
                            </label>
                        </td>
                        <td width="25" height="65" align="left" valign="middle" class="wordright">
                            <img name="img5" width="16" height="16" id="img5" /></td>
                        <td width="170" height="65">
                            <div class="registertip" id="userPasstip">密码由6到20个字母、数字、特殊符号组成，字母区分大小写</div>
                        </td>
                    </tr>
                    <!-- 重复新密码 -->
                    <tr>
                        <td width="120" height="65" align="right"><strong>重复新密码</strong></td>
                        <td width="20" height="65">&nbsp;</td>
                        <td width="210" height="65"><label>
                            <input name="userRPass" type="password" class="n1" id="userRpass" onblur="checkUserRpass(img6,this)" onfocus="getfocus(this,img6)" />
                        </label></td>
                        <td width="25" height="65" align="left" valign="middle" class="wordright">
                            <img name="img6" width="16" height="16" id="img6" />
                        </td>
                        <td width="170" height="65"><div class="registertip" id="userRpasstip">请再次输入密码</div></td>
                    </tr>
                    <!-- 保存按钮 -->
                    <tr>
                        <td width="120" align="right">&nbsp;</td>
                        <td width="20">&nbsp;</td>
                        <td><label>
                            <input name="button" type="submit" class="btn" id="button" value="保存" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#">密码忘了？</a>
                        </label></td>
                    </tr>
                </table>
            </div>
            <!-- bannerLeft部分DIV结束 -->
            <!-- bannerRight部分DIV -->
            <div class="right" id="right">
                <p><strong>关于密码</strong></p>
                <p>设置强度较高的账户密码（建议使用6~20位字母+数字+特殊符号，字幕区分大小写）</p>
                <p>避免使用用户名，连续或相同的数字作为密码 </p>
            </div>
            <!-- bannerRight部分DIV结束 -->
        </div>
        <!-- banner部分DIV结束 -->
    </div>
    <!-- container部分DI结束V -->
</form>
</body>
</html>
