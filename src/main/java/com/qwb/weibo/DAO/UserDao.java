package com.qwb.weibo.DAO;


import com.qwb.weibo.Model.User;

import javax.servlet.http.HttpSession;

/**
 * Created by ThinkPad on 2016/12/19.
 */
public interface UserDao {

    //用户注册
    int addUsers(User user);
    //通过用户名和密码获取用户对象
    User getUserByNameAndPassword(String userName,String passWord);

    User getUserByuserMail(String userName);
    //用户设置
    int account(String userName,String userNick,String domainhacks ,String userMail,
            String region1,String region2,String region3,String personalSite,String sec);

    int password(String password, int id );
//    添加关注
    int starAttention(String Buser_Id,String Id);

    int attentionNumber(String Id);
}
