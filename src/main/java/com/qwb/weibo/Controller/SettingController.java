package com.qwb.weibo.Controller;

import com.qwb.weibo.DAO.UserDao;
import com.qwb.weibo.Model.User;
import com.qwb.weibo.Service.UserService;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2016/12/23 0023.
 */
@Controller
@RequestMapping("/")
public class SettingController {

    @Autowired
    private UserService userService;

    @Autowired
    UserDao userDao;
    //======================== setting设置页面主页和设置子页面=========================================
    @RequestMapping(value = "/setting",method = RequestMethod.GET)
    public String showSetting(Model model,  @RequestParam("intsrc") String intsr){

        model.addAttribute("intsrc", intsr);


        return  "setting";
    }
    //  ---------------------account账号设置（修改用户信息）---------------------------------------------
    @RequestMapping(value = "/account",method = RequestMethod.POST)
    public String account(HttpSession session, Model model,
                          @RequestParam("userName") String userName,
                          @RequestParam("textfield") String userNick,
                          @RequestParam("domainhacks") String  domainhacks,
                          @RequestParam("textfield3") String userMail,
                          @RequestParam("region1") String region1,
                          @RequestParam("region2") String region2,
                          @RequestParam(value = "region3",required = false) String region3,
                          @RequestParam("personalSite") String personalSite ,
                          @RequestParam("sec") String sec){
        //创建User对象
        try{
            int a = userService.account(userName, userNick, domainhacks,userMail,region1,region2,region3,personalSite,sec);
            if (a == 0) {
                //添加失败
                model.addAttribute("errmsg", "修改失败，请重试！");
                return "setting";
            } else {
                //修改成功
                session.removeAttribute("user");
//                session.clear();
                User user = userService.userMail(userName);
                session.setAttribute("Login_User", user);
                return "/setting";
            }

        } catch (Exception e) {

            e.printStackTrace();
            model.addAttribute("errmsg", e.getLocalizedMessage());
            //返回的是view文件名
            return "setting";
        }

    }
    //  ---------------------account账号设置（修改用户信息）---------------------------------------------
    //  ---------------------password密码设置（修改用户密码）---------------------------------------------
    @RequestMapping(value = "/passWord",method = RequestMethod.POST)
    public String password(HttpSession session, Model model){


//        int id = (int) session.getAttribute("id");
//        String password = (String) session.getAttribute("password");
//        int id = request.getSession().getAttribute("id");
//        String password = request.getSession().getAttribute("passWord");
        //创建User对象
//        try{
//            int a = userService.password(password, id);
//            if (a == 0) {
//                //添加失败
//                model.addAttribute("errmsg", "添加失败，请重试！");
//                return "passWord";
//            } else {
//                //添加成功
//                return "/passWord";
//            }
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//            model.addAttribute("errmsg", e.getLocalizedMessage());
//            //返回的是view文件名
//            return "passWord";
//        }

        //返回的是view文件名
        return "passWord";
    }
    //  ---------------------password密码设置（修改用户密码）---------------------------------------------
    //  ---------------------avatar头像设置（上传或修改用户头像）---------------------------------------------

    @RequestMapping(value = "/avatar",method = RequestMethod.POST)
    public String avatar(){

        //返回的是view文件名
        return "/avatar";
    }
    //  ---------------------avatar头像设置（上传或修改用户头像）---------------------------------------------
    //  ---------------------mobile手机绑定设置（绑定手机，用短信/彩信写微博）---------------------------------------------

    @RequestMapping(value = "/mobile",method = RequestMethod.POST)
    public String mobile(){

        //返回的是view文件名
        return "/mobile";
    }
    //  ---------------------mobile手机绑定设置（绑定手机，用短信/彩信写微博）---------------------------------------------
    //  ---------------------design模板选择（模板图片）---------------------------------------------

    @RequestMapping(value = "/design",method = RequestMethod.POST)
    public String design(){

        //返回的是view文件名
        return "/design";
    }
//======================== setting设置页面主页和设置子页面=========================================


}
