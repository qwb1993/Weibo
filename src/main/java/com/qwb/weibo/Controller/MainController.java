package com.qwb.weibo.Controller;

import com.alibaba.fastjson.JSON;
import com.qwb.weibo.DAO.UserDao;
import com.qwb.weibo.Model.User;
import com.qwb.weibo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by ThinkPad on 2016/12/19.
 */
@Controller
@RequestMapping("/")
public class MainController {

    @Autowired private UserService userService;

    @Autowired UserDao userDao;
    //   --------------------显示/提交登录页面-----------------------------------
    //显示登录页面
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showLogin() {
        //返回的是View文件名

        return "login";
    }

    //用户登录
    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String showCustomerIndex(HttpSession session,Model model,
                                    @RequestParam("userName") String userName,
                                    @RequestParam("passWord") String passWord){

        try{
            String errmsg = null;
            //根据输入的用户名和密码查询用户并创建用户对象
            User user = userService.login(userName,passWord);
            String userid = null;
            userid =Integer.toHexString(user.getId());
            int attentNumb = userService.attentNumb(userid);



            //判断用户是否存在
            if( user==null ){
                //用户不存在
                model.addAttribute("errmsg","亲，用户名或密码不存在。请检查用户名与密码重新登录");
                return "/";
            }else{
                //用户存在
                session.setAttribute("attentNumb", attentNumb);
                session.setAttribute("Login_User", user);
                return "redirect:/CustomerIndex";
            }
        } catch (Exception e) {

        }
        return "redirect:/CustomerIndex";
    }
    //   --------------------显示//提交登录页面-------------------------------------
    //   --------------------显示/提交微博注册页面-----------------------------------
    //显示注册页面
    @RequestMapping(value = "/register",method = RequestMethod.GET)
     public String showRegister(){
        //返回的是view文件名

        return "register";
    }
    /**
     *实现用户注册功能
     */
    @RequestMapping(value = "/register",method = RequestMethod.POST)
     public String showdoRegister(HttpSession session, Model model,
                                  @RequestParam("userName") String userName,
                                  @RequestParam("userNick") String userNick,
                                  @RequestParam("userTel") String userTel,
                                  @RequestParam("userMail") String userMail,
                                  @RequestParam("userPass") String passWord,
                                  @RequestParam("region1") String site1,
                                  @RequestParam("region2") String site2,
                                  @RequestParam(value = "region3",required = false) String site3,
                                  @RequestParam("year") String year,
                                  @RequestParam("month") String month,
                                  @RequestParam("date") String date) {
        String errmsg = null;
        //创建User对象
        User user = new User(0,userName, userNick, userTel, userMail, passWord, site1, site2, site3, year, month, date);
        int a = userService.register(user, session);
        if (a == 0) {
            //添加失败
            model.addAttribute("errmsg", "添加失败，请重试！");
            return "register";
        } else {
            //添加成功
            return "redirect:/login";
        }
    }


    //   --------------------显示/提交微博注册页面-------------------------------------
    //   --------------------显示微博主页MyWeb（）-----------------------------------
    /**
     * 显示微博主页customerIndex
     */
     @RequestMapping(value = "/CustomerIndex",method = RequestMethod.GET)
        public String showCustomerIndex(){

         //返回的是view文件名
         return "CustomerIndex";
     }
    @RequestMapping(value = "/CustomerIndex",method = RequestMethod.POST)
    public String customerIndex(){

        //返回的是view文件名
        return "CustomerIndex";
    }
    //   --------------------显示微博主页MyWeb（）-----------------------------------
    //   --------------------显示个人主页MyWeb（）-----------------------------------
    /**
     * 显示个人主页MyWeb
     */
    @RequestMapping(value = "/MyWB",method = RequestMethod.GET)
    public String showMyWB(){

        //返回的是view文件名
        return "/MyWB";
    }
    @RequestMapping(value = "/MyWB",method = RequestMethod.POST)
    public String MyWB(){

        //返回的是view文件名
        return "/MyWB";
    }
//   -------------------- 显示个人主页MyWeb（）-----------------------------------

    //   -------------------- friend发现（我的关注）-----------------------------------
    @RequestMapping(value = "/friend",method = RequestMethod.GET)
    public String showFriend(){

        //返回的是view文件名
        return "/focusonyou";
    }
    @RequestMapping(value = "/friend",method = RequestMethod.POST)
    public String friend(){

        //返回的是view文件名
        return "/friend";
    }
//   -------------------- friend发现（我的关注）-----------------------------------

    //   -------------------- focusonyou关注我（我的粉丝）-----------------------------------
    @RequestMapping(value = "/focusonyou",method = RequestMethod.GET)
    public String showFocusonyou(){

        //返回的是view文件名
        return "/focusonyou";
    }
    @RequestMapping(value = "/focusonyou",method = RequestMethod.POST)
    public String focusonyou(){

        //返回的是view文件名
        return "/focusonyou";
    }
//   -------------------- focusonyou关注我（我的粉丝）-----------------------------------
//   -------------------- focusonyou关注我（我的粉丝）-----------------------------------
@RequestMapping(value = "/star",method = RequestMethod.GET)
public String showStar(){

    //返回的是view文件名
    return "/star";
}

    @RequestMapping(value = "/star",method = RequestMethod.POST)
    public String star( Model model,HttpSession session,
                        @RequestParam("BuserId") String Buser_Id,
                         @RequestParam("id") String Id){
        //返回的是view文件名
        int attentNumb=0;
        String message = null;
        Map<String, String> smap = new HashMap();
        try {
            int star = userService.star(Buser_Id,Id);
            attentNumb = userService.attentNumb(Id);
            session.setAttribute("attentNumb", attentNumb);
            if (attentNumb == 0) {
                //插入不成功
                smap.put("msg", "false");
                message = JSON.toJSONString(smap);
                return message;
            }
            smap.put("msg", "true");
            message = JSON.toJSONString(smap);
            return message;

        } catch (Exception e) {
//            Contants.errmsg = e.getLocalizedMessage();
            return "/star";
        }
    }
//   -------------------- focusonyou关注我（我的粉丝）-----------------------------------



}
