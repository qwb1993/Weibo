package com.qwb.weibo.Service;

import com.qwb.weibo.Model.User;
import com.qwb.weibo.DAO.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * Created by Jason on 2016/12/20.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired UserDao userDao;
    /**
     * 用户注册
     */
    public int register(User user,HttpSession session) {
        //向数据库中添加一条用户记录，返回的是行数
        int ret = userDao.addUsers(user);
        if( ret==0 ){
            //插入失败
            throw new RuntimeException("添加失败");
        }else{
            //将用户保存下来
            session.setAttribute("Login_User",user);
        }
        return ret;
    }

    /**
     * 用户登录
     * @param userName
     * @param passWord
     * @return
     */
    public User login(String userName, String passWord) {

        //根据用户名和密码查询用户记录并创建对象
        User user = userDao.getUserByNameAndPassword(userName,passWord);
        if( user == null){
            //用户不存在，返回异常
            throw new RuntimeException("用户未注册，请先注册再登录！");
        }else{
            //用户存在，返回user对象
            return user;
        }
    }
    /**
     * 用户修改前查询用户信息
     */
    public int account(String userName,String userNick,String domainhacks ,String userMail,
                       String region1,String region2,String region3,String personalSite,String sec){

        int updateUser = 0;
        try{

            updateUser = userDao.account(userName,userNick, domainhacks,userMail,
                    region1,region2, region3, personalSite, sec);
            return updateUser;

        } catch (Exception e){
//        1、生成异常并抛到上一级（MainController）中
            throw new RuntimeException("修改用户信息失败");
        }
//        return updateUser;
    }
    /**
     * 修改用户密码信息
     */
    public int password(String password, int id ){

        int updatePassword = 0;
        try{

            updatePassword = userDao.password(password, id);
            return updatePassword;

        } catch (Exception e){
//        1、生成异常并抛到上一级（MainController）中
            throw new RuntimeException("修改密码信息失败");
        }
//        return updateUser;
    }
    /**
     * 修改用户密码信息，后通过邮箱查询该用户全部信息
     */
    public User userMail(String userName) {

        //根据用邮箱查询用户记录并创建对象
        User user = userDao.getUserByuserMail(userName);
        if( user == null){
            //用户不存在，返回异常
            throw new RuntimeException("用户未注册，请先注册再登录！");
        }else{
            //用户存在，返回user对象
            return user;
        }
    }
    //    添加关注用户ID
    public int star(String Buser_Id,String Id) throws SQLException {
        int star=0;
        try {
            //调用SQL语句，添加关注用户ID
            star = userDao.starAttention(Buser_Id,Id);
             //用户存在，返回user对象
                return star;
        } catch (Exception e) {
            e.printStackTrace();
            return star;
        }
    }
    //    查询用户关注的个数
    public int attentNumb(String Id) throws SQLException {
        int Numb=0;
        try {
            //调用SQL语句，添加关注用户ID
            Numb = userDao.attentionNumber(Id);
            //用户存在，返回user对象
            return Numb;
        } catch (Exception e) {
            e.printStackTrace();
            return Numb;
        }
    }



}
