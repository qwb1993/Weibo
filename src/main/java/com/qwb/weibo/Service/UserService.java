package com.qwb.weibo.Service;

import com.qwb.weibo.Model.User;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * Created by Jason on 2016/12/20.
 */
public interface UserService {

    //增加用户
    int register(User user, HttpSession session);
    //设置用户
    int   account(String userName,String userNick,String domainhacks ,String userMail,String region1,
                String region2,String region3,String personalSite,String sec);

    int password(String password, int id );
    //根据用户名和密码查询用户
    User login(String userName,String passWord);
    //根据用用户名查询用户记录并创建对象
    User userMail(String userName);
    //    添加关注用户ID
    int star(String Buser_Id,String Id) throws SQLException;
    int attentNumb(String Id) throws SQLException;
}
