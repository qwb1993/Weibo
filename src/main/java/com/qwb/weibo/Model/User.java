package com.qwb.weibo.Model;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by ThinkPad on 2016/12/19.
 */
public class User {
    private Integer id;
    private String name;
    private String nick_name;
    private String phone;
    private String mail;
    private String password;
    private String site1;
    private String site2;
    private String site3;
    private String year;
    private String month;
    private String date;

    private String personalSite;
    private String domainhacks;
    private String sec;




    public void setSec(String sec) {
        this.sec = sec;
    }




    public String getSec() {
        return sec;
    }

    public void setPersonalSite(String personalSite) {
        this.personalSite = personalSite;
    }

    public void setDomainhacks(String domainhacks) {
        this.domainhacks = domainhacks;
    }

    public String getPersonalSite() {

        return personalSite;
    }

    public String getDomainhacks() {
        return domainhacks;
    }

    public User(Integer id, String name, String nick_name, String phone, String mail,
                String password, String site1, String site2, String site3, String year,
                String month, String date, String personalSite, String domainhacks, String sec) {
        this.id = id;
        this.name = name;
        this.nick_name = nick_name;
        this.phone = phone;
        this.mail = mail;
        this.password = password;
        this.site1 = site1;
        this.site2 = site2;
        this.site3 = site3;
        this.year = year;
        this.month = month;
        this.date = date;
        this.personalSite = personalSite;
        this.domainhacks = domainhacks;

        this.sec = sec;
    }

    public User(Integer id, String name, String nick_name, String phone, String mail, String password,
                String site1, String site2, String site3, String year, String month, String date) {
        this.id = id;
        this.name = name;
        this.nick_name = nick_name;
        this.phone = phone;
        this.mail = mail;
        this.password = password;
        this.site1 = site1;
        this.site2 = site2;
        this.site3 = site3;
        this.year = year;
        this.month = month;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSite1() {
        return site1;
    }

    public void setSite1(String site1) {
        this.site1 = site1;
    }

    public String getSite2() {
        return site2;
    }

    public void setSite2(String site2) {
        this.site2 = site2;
    }

    public String getSite3() {
        return site3;
    }

    public void setSite3(String site3) {
        this.site3 = site3;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
