-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: lashou
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `goods`
--

LOCK TABLES `goods` WRITE;
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (1,'小猪蛋糕',1999,1998,'       kkk                 ','2016-11-13 11:29:01',7),(2,'蛋糕',77799999,77777,'                        ','2016-11-13 11:29:01',7),(3,'小猪蛋糕',8888,888,NULL,'2016-11-13 11:29:01',7),(4,'小猪蛋糕',555,55,NULL,'2016-11-13 11:29:01',7),(5,'小猪蛋糕',6,6,NULL,'2016-11-13 11:29:01',7),(6,'小猪蛋糕',7,6,NULL,'2016-11-13 11:29:01',8),(7,'小猪蛋糕',666,6,NULL,'2016-11-13 11:29:01',1),(8,'小猪蛋糕',7666,888,NULL,'2016-11-13 11:29:01',2),(9,'tr',666,777,'','2016-11-13 17:09:19',3),(10,'t',5,4,'','2016-11-13 17:22:01',4),(11,'re',88,88,'','2016-11-13 20:57:37',5),(12,'re',88,88,'','2016-11-13 21:03:01',6),(13,'ree',88,88,'','2016-11-13 21:03:21',7),(14,'reee',88,88,'','2016-11-13 21:06:12',8),(15,'oo',998,77,'kkk','2016-11-13 21:08:05',9),(16,'rre',545,566,'','2016-11-13 21:10:49',3),(17,'pp',33,44,'fghgt','2016-11-13 21:12:44',10),(18,'gff',546,4354,'hghh','2016-11-13 21:13:50',11),(19,'654',6767,5656,'hghgvv','2016-11-13 21:15:02',12),(20,'bbbbbbf',7666,67777,'hhhbbbb','2016-11-13 21:16:02',13);
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `shoppingcar`
--

LOCK TABLES `shoppingcar` WRITE;
/*!40000 ALTER TABLE `shoppingcar` DISABLE KEYS */;
INSERT INTO `shoppingcar` VALUES (1,19,NULL,1),(2,19,NULL,1),(3,18,NULL,1),(4,19,NULL,1),(5,19,NULL,1),(6,20,NULL,1),(7,19,NULL,1),(8,20,NULL,1);
/*!40000 ALTER TABLE `shoppingcar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subcatalog1`
--

LOCK TABLES `subcatalog1` WRITE;
/*!40000 ALTER TABLE `subcatalog1` DISABLE KEYS */;
INSERT INTO `subcatalog1` VALUES (1,'美食'),(2,'休闲娱乐'),(3,'生活服务'),(4,'摄影写真'),(5,'酒店'),(6,'旅游'),(7,'丽人');
/*!40000 ALTER TABLE `subcatalog1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subcatalog2`
--

LOCK TABLES `subcatalog2` WRITE;
/*!40000 ALTER TABLE `subcatalog2` DISABLE KEYS */;
INSERT INTO `subcatalog2` VALUES (1,'中餐',1),(2,'西餐',1),(3,'火锅',1),(4,'烧烤',1),(5,'KTV',2),(6,'养生',2),(7,'温泉洗浴',2),(8,'运动健身',2),(9,'桌游电玩',2),(10,'咖啡',2),(11,'茶馆',2),(12,'展览',2),(13,'4D/5D电影',2),(14,'健康护理',3),(15,'汽车服务',3),(16,'母婴亲子',3),(17,'洗涤护理',3),(18,'婚庆服务',3),(19,'宠物护理',3),(20,'缴费充值',3),(21,'照片冲印',3),(22,'其他生活',3),(23,'品质婚纱',4),(24,'热卖婚纱',4),(25,'孕妇写真',4),(26,'成人写真',4),(27,'证件照',4),(28,'照片冲印',4),(29,'结婚婚庆',4),(30,'经济型酒店三星',5),(31,'舒适四星',5),(32,'高档五星',5),(33,'豪华特色酒店',5),(34,'小时房',5),(35,'周边游门',6),(36,'国内游',6),(37,'国外游',6),(38,'温泉嬉水',6),(39,'农家乐',6),(40,'美容',7),(41,'美体',7),(42,'美发',7),(43,'美甲',7),(44,'瑜珈/舞蹈',7),(45,'公司简介',NULL),(46,'企业文化',NULL),(47,'组织结构',NULL),(48,'客服服务',NULL);
/*!40000 ALTER TABLE `subcatalog2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subcatalog3`
--

LOCK TABLES `subcatalog3` WRITE;
/*!40000 ALTER TABLE `subcatalog3` DISABLE KEYS */;
INSERT INTO `subcatalog3` VALUES (1,'川菜',1),(2,'粤菜',1),(3,'东北菜',1),(4,'鲁菜',1),(5,'江浙菜',1),(6,'传统滋补',1),(7,'蛋糕',2),(8,'面包',2),(9,'披萨',2),(10,'牛排',2),(11,'肯德基',2),(12,'必胜客',2),(13,'麦当劳',2),(14,'四川火锅',3),(15,'老北京涮肉',3),(16,'东北白肉火锅',3),(17,'苏杭菊花火锅',3),(18,'云南滇味火锅',3),(19,'湖南腊味火锅',3),(20,'广东海鲜火锅',3);
/*!40000 ALTER TABLE `subcatalog3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'a','a@a.com',1,NULL,NULL,'a',0,'2016-12-02 09:44:10'),(2,'b','b@b.com',0,'bbbbbbbb',NULL,'b',1,'2016-12-02 09:44:10'),(3,'we','',1,'','','ww',0,'2016-12-02 09:44:10'),(5,'weq','qwe@werfwe.cfg',0,'edfsdf','231232432','ww',0,'2016-12-02 09:44:10'),(7,'jj','ww@ii.gugu',1,'erffdghhh','12695855','oo',0,'2016-12-02 09:44:10'),(8,'ee','qq@gg.cc',1,'reter','12432423','ll',0,'2016-12-02 09:44:10'),(9,'tt','dd',0,'eee','18766654','jj',0,'2016-12-02 09:44:10'),(18,'vv','135@ee.dfsd',0,'fdfd','11112','vv',0,'2016-12-18 19:25:40'),(19,'x','sss@ddd.dfd',0,'sddd','6666','x',1,'2016-12-18 19:32:08'),(20,'n','123@43rt.ty',0,'fds','324234','n',1,'2016-12-18 19:34:09'),(22,'fdsf','111223@erwe.4',0,'reef','1232434','ewrer',0,'2016-12-18 19:36:18'),(25,'c','123@uy.rtr',0,'dfgdf','1565343','c',1,'2016-12-18 19:39:20'),(26,'d','123@u.u',0,'fgf','13434','d',0,'2016-12-18 19:43:45'),(27,'e','123@e.e',0,'eee','12346','e',0,'2016-12-18 19:48:47'),(28,'f','143@rr.yy',0,'eef','12434532','f',1,'2016-12-18 19:57:02'),(29,'g','213@tre.fdg',0,'eredf','342','g',1,'2016-12-18 20:00:17'),(30,'h','12eee',0,'ff','223231','h',0,'2016-12-18 20:09:21'),(31,'j','ee',0,'gg','12344','j',1,'2016-12-18 20:11:05'),(32,'k','ff',0,'kk','443','k',0,'2016-12-18 20:13:51'),(34,'m','fgdf',0,'ffdg','34535','m',1,'2016-12-18 20:20:11'),(36,'aa','ss',0,'ss','33','aa',1,'2016-12-18 20:23:22'),(37,'asd','asd',0,'asd','45345345','asd',1,'2016-12-18 20:26:51');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-22 10:30:25
